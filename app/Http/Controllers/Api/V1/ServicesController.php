<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\Services;
use App\Models\Messages;
use Illuminate\Http\Request;

class ServicesController extends Controller
{
    /**
     * @api {get} /services/index Solicitar información de Servicios
     * @apiVersion 1.1.0
     * @apiDescription Solicitud de todos los servicios publicados y mensajes realizados a cada servicio.
     * @apiName GetServicios
     * @apiGroup Servicios
     *
     * @apiParam {Number} [idservice] ID del servicio a consulta para paginacion de mensajes.
     * @apiParam {Number} [pag] Numero de Pagina para Listar Mensajes.
     *
     * @apiSuccess {Number} service.id ID unico que identifica al Servicio publicado.
     * @apiSuccess {String} title Titulo que se creo para identificar el Servicio.
     * @apiSuccess {String} description Detalles descriptivos del Servicio publicado.
     * @apiSuccess {Number} amount Importe o costo del servicio publicado.
     * @apiSuccess {String} modality Modalidad o tipo de servicio que se Ofrece, si es Remoto o Presencial.
     * @apiSuccess {Number} users_id ID unico del usuario que publica el Servicio.
     * @apiSuccess {Date} created_at Fecha de Creacion o publicacion del Servicio
     * @apiSuccess {Date} updated_at Fecha de Actualiacion del Servicio.
     * 
     * @apiSuccess {Number} messages.id ID del mensaje publicado por un Servicio.
     * @apiSuccess {Number} users_id ID unico del usuario que envio el mensaje.
     * @apiSuccess {String} message Cuerpo del mensaje enviado por el usuario.
     * @apiSuccess {String} [file] Nombre del archivo adjuntado por el usuario que envia el mensaje.
     * @apiSuccess {Date} messages.created_at Fecha de Creacion del Mensaje.
     * @apiSuccess {Date} messages.updated_at Fecha de Actualiacion del Mensaje.
     * 
     * @apiSuccess {Number} page Pagina actual en la que se muestran los mensajes.
     * @apiSuccess {Number} page_size Tamaño del listado de mensajes por pagina.
     * @apiSuccess {Number} total_pag Total de la cantidad de paginas de mensajes.
     * @apiSuccess {Number} total_results Total de mensajes totales que se encuentran por Servicio.
     * 
     * @apiSuccessExample {json} Success-Servicios
     *      HTTP/1.1 200 OK
     *
     *       {
     *       "service": {
     *                       "id": 2,
     *                       "title": "Aprende desarrollo de Software",
     *                       "description": "Aprende Desarrollo de Software con Profesor, con amplia experiencia en el Desarrollo Web ",
     *                       "amount": 11.5,
     *                       "modality": "remote",
     *                       "users_id": 2,
     *                       "created_at": "2022-12-06T21:55:42.000000Z",
     *                       "updated_at": "2022-12-06T21:55:42.000000Z"
     *                       },
     *       "messages": [
     *                       {
     *                       "id": 5,
     *                       "users_id": 1,
     *                       "message": "Hola DANIEL485 se puede mejorar un precio por 10 horas?",
     *                       "file": null,
     *                       "created_at": "2022-12-06T21:55:42.000000Z",
     *                       "updated_at": "2022-12-06T21:55:42.000000Z"
     *                       }
     *       ],
     *       "page": 1,
     *       "page_size": 15,
     *       "total_pag": 1,
     *       "total_results": 1
     *       }
     *
     */
    public function index(Request $request)
    {
        try {
            $response = array();

            // Tipo de consulta de listado de Servicios
            if (isset($request->idservice)) {
                $services = Services::where('id', $request->idservice)
                                    ->orderBy('created_at', 'DESC')
                                    ->get();
            } else {
                $services = Services::orderBy('created_at', 'DESC')->get();
            }


            foreach ($services as $service) {
                // Paginacion
                $msg_page = 15;
                $pag = $request->pag;
                if (empty($pag)) {
                    $first = 0;
                    $pag = 1;
                } else {
                    $first = ($pag - 1) * $msg_page;
                }

                $messages = Messages::where('services_id', $service->id)
                                        ->orderBy('created_at', 'DESC')
                                        ->skip($first)
                                        ->take($msg_page)
                                        ->get();

                // Contador Mensajes
                $count_msg = count(Messages::where('services_id', $service->id)->get());

                $total_pag = ceil($count_msg / $msg_page);

                $response[] = array('service' => $service,
                                    'messages' => $messages,
                                    'page' => empty($count_msg) ? 0 : $pag,
                                    'page_size' => $msg_page,
                                    'total_pag' => $total_pag,
                                    'total_results' => $count_msg);
            }

            return response()->json($response, 200);
        } catch (\Throwable $th) {
            return response()->json(["data" => $th], 422);
        }
    }
}
