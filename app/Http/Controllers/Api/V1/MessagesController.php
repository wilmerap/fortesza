<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Services;
use App\Models\Messages;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Str;

class MessagesController extends Controller
{
    /**
     * @api {post} /services/:id/newmsg Enviar Informacion del Mensaje.
     * @apiVersion 1.1.0
     * @apiDescription Registro del mensaje enviado por el usuario visitante referente a un Servicio Publicado.
     * @apiName setMensajes
     * @apiGroup Mensajes
     *
     * @apiParam {Number} id ID único de servicio publicado.
     *
     * @apiBody {String} username Nombre usuario que realiza la pregunta del servicio (Ej. MARIA_1234).
     * @apiBody {String} message Mensaje o pregunta que se realiza del servicio.
     * @apiBody {File} [file] Envio de archivo “Foto, Audio”, para complementar el mensaje.
     *
     * @apiSuccess {Boolean} created Indicador de registro del mensaje.
     * @apiSuccess {Number} id ID Generada del Nuevo Mensaje Procesado.
     * @apiSuccess {String} status El resultado del proceso de la Creacion del Mensaje.
     *
     * @apiSuccessExample {json} Success-Mensaje-Enviado
     *      HTTP/1.1 200 OK
     *      {
     *         "created": true,
     *         "id": "1",
     *         "status": "OK"
     *      }
     *
     * @apiError RequestNotFound  El <code>username</code> username <code>message</code> message campos requeridos.
     * @apiError FileError  <code>data</code> data Mensaje de error al intentar procesar Archivo Adjunto.
     *
     * @apiSuccessExample {json} Success-username-Error
     *      HTTP/1.1 200 OK
     *      {
     *         "created": false,
     *         "errors": [
     *              "The username field is required."
     *         ]
     *      }
     *
     * @apiSuccessExample {json} Success-message-Error
     *      HTTP/1.1 200 OK
     *      {
     *         "created": false,
     *         "errors": [
     *              "The message field is required."
     *         ]
     *      }
     *
     */
    public function store(Request $request)
    {
        // Reglas de Validacion request
        $rules = [
            'username'     => 'required',
            'message'  => 'required'
            ];

        // Validador de Request
        $validator = \Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return [
                'created' => false,
                'errors'  => $validator->errors()->all()
            ];
        }

        // Validacion de Existencia
        try {
            $service = Services::findOrFail($request->service_id);

            $user = User::where('name', '=', $request->username)->first();
        } catch(ModelNotFoundException $e) {
            return response()->json(["data" => $e], 422);
        }

        try {
            // Archivos Adjuntos
            if (isset($request->file)) { 

                $file = new FileUploadMsg($request->file);
                $fileName = $file->processFile();

            }

            // Registro del Mensaje
            $post = new Messages();
            $post->services_id = $service->id;
            $post->users_id = $user->id;
            $post->message = $request->message;
            $post->file = (isset($fileName)) ? $fileName : null;
            $post->ip = $request->ip();
            $post->save();

            if (isset($post->id)) {
                $response = array('created' => true, 'id' => $post->id, 'status' => 'OK');
            } else {
                $response = array('status' => 'ERROR');
            }

            return response()->json($response, 200);
        } catch (\Throwable $th) {
            return response()->json(["data" => $th], 422);
        }
    }
}

class FileUploadMsg
{
    private $extFile;
    private $nameFile;
    private $nameTmp;
    private $fileName;

    public function __construct($file)
    {
        // Informacion del Archivo
        $this->nameFile = $file->getClientOriginalName();
        $this->nameTmp = $file->path();
        $this->extFile = $file->getClientOriginalExtension();
        $this->fileName = Str::random(20).'.'.$this->extFile;
    }

    public function processFile()
    {
        // Define Ubicacion del Storage
        $path = storage_path('app/public/'.$this->fileName);

        if (!move_uploaded_file($this->nameTmp, $path)) {
            return response()->json(["data" => 'Error al Procesar el Archivo'], 422);
            die();
        }

        return $this->fileName;
    }
    
}

