<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Messages extends Model
{
    use HasFactory;

    protected $table = 'messages_services';

    protected $primaryKey = 'id';

    protected $hidden = ['ip','services_id'];
}
