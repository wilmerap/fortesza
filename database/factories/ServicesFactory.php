<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Services>
 */
class ServicesFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'title' => 'Aprende desarrollo de Software',
            'description' => 'Aprende Desarrollo de Software con Profesor, con amplia experiencia en el Desarrollo Web ',
            'amount' => rand(1, 15).'.50',
            'modality' => fake()->randomElement(['remote', 'onsite']),
            'users_id' => \App\Models\User::inRandomOrder()->value('id'),
            'created_at' => now(),
        ];
    }
}
