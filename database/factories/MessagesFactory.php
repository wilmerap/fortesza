<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class MessagesFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'message' => 'Hola '.\App\Models\User::inRandomOrder()->value('name').' se puede mejorar un precio por '.rand(2, 15).' horas?',
            'services_id' => \App\Models\Services::inRandomOrder()->value('id'),
            'users_id' => \App\Models\User::inRandomOrder()->value('id'),
            'ip' => '127.0.0.1',
            'created_at' => now(),
        ];
    }
}
