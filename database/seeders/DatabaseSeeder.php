<?php

namespace Database\Seeders;

use Illuminate\Support\Facades\DB;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::factory()->create([
            'name' => 'DANIEL485',
            'email' => 'daniel@example.com',
        ]);

        \App\Models\User::factory()->create([
           'name' => 'MARIA_1234',
           'email' => 'maria_1234@example.com',
        ]);

        \App\Models\Services::factory(5)->create();

        \App\Models\Services::factory()->create([
           'title' => 'Aprende desarrollo de Software',
           'description' => 'Aprende Desarrollo de Software con Profesor, con amplia experiencia en el Desarrollo Web ',
           'amount' => 10.50,
           'modality' => 'remote',
           'users_id' => 1,
        ]);

        \App\Models\Messages::factory(5)->create();
    }
}
