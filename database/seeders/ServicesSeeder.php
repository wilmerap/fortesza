<?php

namespace Database\Seeders;

use Illuminate\Support\Facades\DB;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ServicesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('services')->insert([
            'title' => 'Aprende desarrollo de Software',
            'description' => 'Aprende Desarrollo de Software con Profesor, con amplia experiencia en el Desarrollo Web ',
            'amount' => 10.50,
            'modality' => 'remote',
            'users_id' => 1,
        ]);
    }
}
