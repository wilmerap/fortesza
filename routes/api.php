<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group([
    'prefix' => 'services',
], function ($router) {
    Route::get('/index', [\App\Http\Controllers\Api\V1\ServicesController::class, 'index'])->name('list.services');
    Route::post('/{service_id}/newmsg', [\App\Http\Controllers\Api\V1\MessagesController::class, 'store'])->name('files.store');
});
