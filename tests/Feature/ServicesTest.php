<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ServicesTest extends TestCase
{
    /**
     * A basic feature test services.
     *
     * @return void
     */

    /** @test */
    public function get_all_services()
    {

        $response = $this->get('/api/services/index');

        $response->assertStatus(200);

        $response->assertJsonStructure([
            '*' => [
                'service',
                'messages',
                'page',
                'page_size',
                'total_pag',
                'total_results'
            ]
        ]);

    }

    /** @test */
    public function get_service()
    {

        $response = $this->get('/api/services/index?idservice=3');

        $response->assertStatus(200);

        $response->assertJsonStructure([
            '0' => [
                'service',
                'messages',
                'page',
                'page_size',
                'total_pag',
                'total_results'
            ]
        ]);

    }

    /** @test */
    public function get_service_pag_messages()
    {

        $response = $this->get('/api/services/index?idservice=3&pag=1');

        $response->assertStatus(200);

        $response->assertJsonStructure([
            '0' => [
                'service',
                'messages',
                'page',
                'page_size',
                'total_pag',
                'total_results'
            ]
        ]);

    }
}
