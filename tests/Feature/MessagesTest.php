<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use App\Models\User;
use App\Models\Messages;
use App\Models\Services;

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\UploadedFile;

class MessagesTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_response()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    /** @test */
    public function message_post_create()
    {

        // Params
        $msgPost = 'Test2';
        $userNamePost = 'MARIA_1234';

        $service = Services::inRandomOrder()->value('id');

        $response = $this->post('/api/services/'.$service.'/newmsg', [
            'username' => $userNamePost,
            'message' => $msgPost
        ]);

        $response->assertJsonFragment(['created' => true]);

        $user = User::where('name', '=', $userNamePost)->first();

        $msg = Messages::where('message', '=', $msgPost)->where('users_id', '=', $user->id)->first();

        $this->assertEquals($msg->users_id, $user->id);
        $this->assertEquals($msg->message, $msgPost);


    }

    /** @test */
    public function message_post_error_msg_empty_create()
    {

        // Params
        $msgPost = '';
        $userNamePost = 'MARIA_1234';

        $service = Services::inRandomOrder()->value('id');

        $response = $this->post('/api/services/'.$service.'/newmsg', [
            'username' => $userNamePost,
            'message' => $msgPost
        ]);

        $response->assertJsonFragment(['created' => false]);

    }

    /** @test */
    public function message_post_error_username_empty_create()
    {

        // Params
        $msgPost = 'Test';
        $userNamePost = '';

        $service = Services::inRandomOrder()->value('id');

        $response = $this->post('/api/services/'.$service.'/newmsg', [
            'username' => $userNamePost,
            'message' => $msgPost
        ]);

        $response->assertJsonFragment(['created' => false]);

    }

    /** @test */
    public function message_post_error_username_notfound()
    {

        // Params
        $msgPost = 'Test';
        $userNamePost = 'WILMER_1234';

        $service = Services::inRandomOrder()->value('id');

        $response = $this->post('/api/services/'.$service.'/newmsg', [
            'username' => $userNamePost,
            'message' => $msgPost
        ]);

        $response->assertUnprocessable();

    }

    /** @test */
    public function message_post_file_create()
    {

        Storage::fake('avatars');

        // Params
        $msgPost = 'Test File Send';
        $userNamePost = 'MARIA_1234';

        $service = User::inRandomOrder()->value('id');

        $response = $this->post('/api/services/'.$service.'/newmsg', [
            'username' => $userNamePost,
            'message' => $msgPost,
            'file' => UploadedFile::fake()->image('avatar.jpg')
        ]);

        $response->assertJsonFragment(['created' => true]);

        $user = User::where('name', '=', $userNamePost)->first();

        $msg = Messages::where('message', '=', $msgPost)->where('users_id', '=', $user->id)->first();

        $this->assertEquals($msg->users_id, $user->id);
        $this->assertEquals($msg->message, $msgPost);


    }
}
